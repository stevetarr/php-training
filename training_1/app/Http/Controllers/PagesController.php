<?php

namespace App\Http\Controllers;

use App\Example;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Filesystem\Filesystem;

class PagesController extends Controller
{
    public function home(Filesystem $file)
    {
        return $file->get(public_path('index.php'));

        //return View::make('welcome');     //These two are the
        //return view('welcome');           //same exact thing
    }
}
