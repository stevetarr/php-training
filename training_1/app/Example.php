<?php


namespace App;


class Example
{

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function handle()
    {
        die('It works!');
    }
}
