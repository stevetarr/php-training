<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

app()->bind('App\Example', function () {
    $collaborator = new \App\Collaborator();
    $foo = 'foobar';

   return new \App\Example($collaborator, $foo);
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', 'PagesController@home');

Route::get('/about', function() {
    return view('about',    [
        'articles' => App\Article::take(3)->latest()->get()
    ]);
});

Route::get('payments/create', 'PaymentsController@create')->middleware('auth');
Route::post('payments', 'PaymentsController@store')->middleware('auth');
Route::get('notifications', 'UserNotificationsController@show')->middleware('auth');

Route::get('/contact', 'ContactController@show');
Route::post('/contact', 'ContactController@store');


//  GET /articles
//  GET /articles/:id
//  POST /articles
//  PUT /articles/:id
//  DELETE /articles/:id/

Route::get('/articles', 'ArticlesController@index')->name('articles.index');                    //index
Route::post('/articles', 'ArticlesController@store')->name('articles.store');                   //store
Route::get('/articles/create', 'ArticlesController@create');            //create
Route::get('/articles/{article}', 'ArticlesController@show')->name('articles.show');          //show
Route::get('/articles/{article}/edit', 'ArticlesController@edit');      //edit
Route::put('/articles/{article}', 'ArticlesController@update');         //update




/*
Route::get('test', function()   {
    return view('test', [
        'name' => request('name')
    ]);
});

*/
/*Route::get('/posts/{post}', function($post) {
    $posts = [
        'my-first-post' => 'Hello, this is my first blog post!',
        'my-second-post' => 'Now I am getting the hang of this blogging thing.'
    ];

    if (! array_key_exists($post, $posts)) {
        abort(404, 'Sorry that post was not found.');
    }
    return view('post', [
        'post' => $posts[$post]
    ]);
});
*/

Route::get('/posts/{post}', 'PostsController@show');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
