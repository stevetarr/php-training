<!DOCTYPE html>
<html class="h-full">
<head>
    <meta
        http-equiv="Content-Type"
        content="text/html;charset=UTF-8"
    >

    <title>Notification Lesson</title>

    <link
        href="http://unpkg.com/tailwindcss@^]1.0/dist/tailwind.min.css"
        rel="stylesheet"
    >
</head>

<body class="bg-gray-100 flex items-center justify-center h-full">
<form
    method="POST"
    action="/payments"
    class="bg-white p-6 rounded shadow-md"
    style="width: 300px"
>
    @csrf

    <button
        type="submit"
        class="bg-blue-500 py-2 text-white rounded-full text-sm w-full"
    >
        Make Payment
    </button>

    @if (session('message'))
        <p class="text-green-500 text-xs">
            {{   session('message')  }}
        </p>
    @endif
</form>
</body>
</html>
