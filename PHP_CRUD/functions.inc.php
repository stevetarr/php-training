<!-- Oft-used functions, like redirect. -->
<?php
require_once 'dbconnect.inc.php';

function clean_input($data)
{
    $data = trim($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>
