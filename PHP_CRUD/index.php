 <!-- Listing page. Lists all records.  -->
<?php $title = "Index";
$page = "Index";
$header = 'Index';
include 'top.inc.php';
$exnum = 'Index';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr';
require_once 'dbconnect.inc.php';

session_start();

if (!empty($_SESSION['msg']))    {
    echo $_SESSION['msg'] . "<br/><br/>";
}

?>

    <table class="testTable">
        <tr>
            <th>ID</th>
            <th>Movie Title</th>
            <th>Synopsis</th>
            <th>Release Date</th>
            <th>Rating</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <?php
        //Print out data from the results as new TR rows with TDs for each column
        while  ($row = mysqli_fetch_assoc($result))    {
            print "<tr>";
            print "<td>" . $row[`id`]  .   "</td>";
            print "<td>" . $row[`movie_title`]  .   "</td>";
            print "<td>" . $row[`synopsis`]  .   "</td>";
            print "<td>" . $row[`release_date`]  .   "</td>";
            print "<td>" . $row[`rating`]  .   "</td>";
            print "<td><a href = 'edit.php?id=" . $row[`id`] . "'>Edit  </a></td>";
            print "<td><a href = 'delete.php?id=" . $row[`id`]  . "'>Delete</a></td>";
            print "</tr>";
        }
        ?>
    </table>

    <br/><button type="button" onclick="window.location.href = 'add.php'">Add New</button><br/><br/>
<?php include 'bottom.inc.php'; ?>
