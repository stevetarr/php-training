<!-- Edit form. Preloaded with data from database.  -->
<?php $title = "Movie Release Information";
$page = "Edit";
$header = 'Edit Movie';
$exnum = 'Edit';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr';
include 'top.inc.php';

//  Set up db connection
require_once 'dbconnect.inc.php';

//  Start session, so we can use $_SESSION
session_start();

//  Get querystring variable, test for any problems.
$id = $_GET['id'];
if (! is_numeric($id) || $id < 1)   {
    //  If there is a problem, set a message and redirect.
    $msg = "Invalid ID given.";
    $_SESSION['msg'] = $msg;
    header("Location: index.php");
    exit();
}

//  Prep variable for SQL;
$idSql = clean_input($id);

//  SQL to get record
$stmt = $dbLink->prepare("SELECT id, movie_title, synopsis, release_date, rating
                                 FROM movies WHERE id=?");
$stmt->bind_param('d', $idSql);

//  Run SQL and get resource
$stmt->execute();
$result = $stmt->get_result();


//  Test to see if record is OK.
if (!$result) {
    $msg = "Error retrieving record. MySQLI Error: " .mysqli_error();
    $_SESSION['msg'] = $msg;
    header("Location: index.php");
    exit();
}

//  Check to see if record even exists!
if (mysqli_num_rows($result) != 1)  {
    $msg = "Could not find record $id";
    $_SESSION['msg'] = $msg;
    header("Location: index.php");
    exit();
}

$movie = mysqli_fetch_assoc($result);

$movie_title = $synopsis = $release_date = $rating = "";
$titleErr = $synopErr = $dateErr = "";

if  ($_SERVER["REQUEST_METHOD"] == "POST")  {
    if(empty($_POST["movie_title"]))    {
        $titleErr = "Movie Title is required!";
    }   else {
        $movie_title = clean_input($_POST['movie_title']);

    }

    if(empty($_POST["synopsis"]))   {
        $synopErr = "Synopsis is required!";
    }   else {
        $synopsis = clean_input($_POST['synopsis']);
    }

    if(empty($_POST["$release_date"]))  {
        $dateErr = "";
    }
}
?>

<form action="editDo.php" method="post">
    <p><span class="error">* required field</span><br/>
        <label for="movie_title">Movie Title:</label>
        <input type="text" id="movie_title" name="movie_title" value="<?php echo htmlspecialchars($movie['movie_title']) ?>" maxlength="80" size="40" />
        <span class="error">*<?php echo $titleErr; ?></span>
        <input type="hidden" id="id" name="id" value="<?php echo htmlspecialchars($movie['id']) ?>" />
    </p>

    <p>
        <label for="synopsis">Synopsis:</label>
    </p>
    <textarea name="synopsis" id="synopsis" rows="5" cols="40"><?php echo htmlspecialchars($movie['synopsis'])?></textarea>
    <span class="error">*<?php echo $synopErr; ?></span>

    <p>
        <label for="release_date">Release Date: </label>
        <input class="textbox-n" type="date" id="release_date" name="release_date" value="<?php echo htmlspecialchars($movie['release_date']) ?>" />
        <span class="error">*<?php echo $dateErr; ?></span>
    </p>

    <p>
        <label for="rating">Rating: </label>
        <select name="rating" id="rating">
            <option value="1" <?php echo $movie['rating'] == 1 ? 'selected="selected"' : ''?>>1 - Very Poor</option>
            <option value="2" <?php echo $movie['rating'] == 2 ? 'selected="selected"' : ''?>>2 - Poor</option>
            <option value="3" <?php echo $movie['rating'] == 3 ? 'selected="selected"' : ''?>>3 - OK</option>
            <option value="4" <?php echo $movie['rating'] == 4 ? 'selected="selected"' : ''?>>4 - Good</option>
            <option value="5" <?php echo $movie['rating'] == 5 ? 'selected="selected"' : ''?>>5 - Great!</option>
        </select>
    </p>
    <input type="submit" name="submit" value="Edit Movie"/>
</form><br/>
<button type="button" onclick="window.location.href = 'index.php'">Cancel</button>

<?php include 'bottom.inc.php'; ?>