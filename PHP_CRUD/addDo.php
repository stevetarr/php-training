<!-- Add form's action and processing page. -->
<?php $title = "Movie Release Information";
$page = "Add Do";
$header = 'Add a New Movie';
$exnum = 'Add Do';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr';
include 'top.inc.php';

//  Set up db connection
require_once 'dbconnect.inc.php';

//  Start session, so we can use $_SESSION
session_start();

//  Prepare vars
if  ($_SERVER["REQUEST_METHOD"] == "POST")  {
    //Required sanitization
    $id = clean_input($_POST['id']);
    $movie_title = clean_input($_POST['movie_title']);
    $synopsis = clean_input($_POST['synopsis']);
    $release_date = clean_input($_POST['release_date']);
    $rating = clean_input($_POST['rating']);
}

//  Build SQL Statements
$stmt = $dbLink->prepare("INSERT INTO movies (`movie_title`, `synopsis`, `release_date`, `rating`)
                            VALUES (?, ?, ?, ?)");

$stmt->bind_param('sssd', $movie_title, $synopsis, $release_date, $rating);

$ok = $stmt->execute();

//  Test, and prep a message
if ($ok)    {
    //  Get the newly inserted ID value from
    $id = $dbLink->insert_id;
    //  Prep a message
    $msg = "Record # $id added!";
}   else    {
    $msg = "Error creating record. MySQLI Error: ". mysqli_error();
}

$stmt->close();

$_SESSION['msg'] = $msg;
header("Location: index.php");
exit();

include 'bottom.inc.php'; ?>
