<!-- Edit form's action and processing page.    -->
<?php $title = "Movie Release Information";
$page = "Edit Do";
$header = 'Edit Movie';
include 'top.inc.php';
$exnum = 'Edit Do';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr';

//  Set up db connection
require_once 'dbconnect.inc.php';

//  Start session, so we can use $_SESSION
session_start();

//  Prepare variables, AND prevent SQL injection
if  ($_SERVER["REQUEST_METHOD"] == "POST")  {
    //Required sanitization
    $id = clean_input($_POST['id']);
    $movie_title = clean_input($_POST['movie_title']);
    $synopsis = clean_input($_POST['synopsis']);
    $release_date = clean_input($_POST['release_date']);
    $rating = clean_input($_POST['rating']);
}

//  Build SQL Statement
$stmt = $dbLink->prepare("UPDATE movies 
                                 SET `movie_title` = ?, `synopsis` = ?, `release_date` = ?, `rating` = ?
                                 WHERE `id` = ?");
$stmt->bind_param('sssdd', $movie_title, $synopsis, $release_date, $rating, $id);

//  Run the SQL: UPDATE statements return true
$ok = $stmt->execute();

//Test, and prep a message
if ($ok)    {
    //  Prep message
    $msg = "Record # $id saved!";
    //$msg = "Error saving record. MySQLI Error: " . mysqli_error();
}   else    {
    $msg = "Error saving record. MySQLI Error: " . mysqli_error();
    //$msg = "Record # $id saved!";
}

$_SESSION['msg'] = $msg;
header("Location: index.php");
exit();

include 'bottom.inc.php'; ?>