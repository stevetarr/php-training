<!-- The beginning of any HTML page (Doctype, head, etc)    -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"  />
    <title><?php echo $title ?></title>
    <style type = "text/css">
        table  {
            width: 100%;
        }

        table, th, td   {
            border: 1px solid black;
        }
        .error   {
            color: red;
        }
    </style>
</head>
<body>
    <h1><?php echo $header ?></h1>
    <?php include 'functions.inc.php';  ?>
