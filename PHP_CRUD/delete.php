<!-- Delete confirmation page.  -->
<?php $title = "Delete Movie";
$page = "Delete Movie";
$header = 'Delete Movie';
include 'top.inc.php';
$exnum = 'Delete Movie';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr';

//  Set up db connection
require_once 'dbconnect.inc.php';

//  Start session, so we can use $_SESSION
session_start();

//  Get querystring variable, test for any problems.
$id = $_GET['id'];
if (! is_numeric($id) || $id < 1)   {
    //  If there is a problem, set a message and redirect.
    $msg = "Invalid ID given.";
    $_SESSION['msg'] = $msg;
    header("Location: index.php");
    exit();
}

//  Prep variable for SQL;
$idSql = clean_input($id);

//  SQL to get record
$stmt = $dbLink->prepare("SELECT id, movie_title, synopsis, release_date, rating
                                 FROM movies WHERE id=?");
$stmt->bind_param('d', $idSql);

//  Run SQL and get resource
$stmt->execute();
$result = $stmt->get_result();


//  Test to see if record is OK.
if (!$result) {
    $msg = "Error retrieving record. MySQLI Error: " .mysqli_error();
    $_SESSION['msg'] = $msg;
    header("Location: index.php");
    exit();
}

//  Check to see if record even exists!
if (mysqli_num_rows($result) != 1)  {
    $msg = "Could not find record $id";
    $_SESSION['msg'] = $msg;
    header("Location: index.php");
    exit();
}

$movie = mysqli_fetch_assoc($result);
?>

<form action="deleteDo.php" method="post">
    <p>
        <?php
        echo "<br/> Movie Title:   <strong><br/>" . $movie['movie_title'] . "</strong><br/>";
        echo "<br/> Synopsis:  <strong><br/>" . $movie['synopsis'] . "</strong><br/>";
        echo "<br/> Release Date:  <strong><br/>" . $movie['release_date'] . "</strong><br/>";
        echo "<br/> Rating:        <strong><br/>" . $movie['rating'] . "</strong><br/>";
        ?>
        <input type="hidden" id="id" name="id" value="<?php echo htmlspecialchars($movie['id']) ?>" />
        <br/><br/><strong> Are you sure you want to delete this record?! </strong><br/><br/>
        <input type="submit" name="submit" value="Yes, delete!"/>
    </p>
</form>
<button type="button" onclick="window.location.href = 'index.php'">No, don't delete!</button>
<?php include 'bottom.inc.php'; ?>