<!-- Delete confirmation's action and processing page.  -->
<?php $title = "Delete Do";
$page = "Delete Do";
$header = 'Delete Do';
include 'top.inc.php';
$exnum = 'Delete Do';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr';

//  Set up db connection
require_once 'dbconnect.inc.php';

//  Start session, so we can use $_SESSION
session_start();

//  Prepare variables, AND prevent SQL injection
if  ($_SERVER["REQUEST_METHOD"] == "POST")  {
    //Required sanitization
    $id = clean_input($_POST['id']);
}

$stmt = $dbLink->prepare("DELETE FROM movies WHERE `id` = ?");
$stmt->bind_param('d', $id);

$ok = $stmt->execute();

//Test, and prep a message
if ($ok)    {
    //  Prep message
    $msg = "Record # $id deleted!";
}   else    {
    $msg = "Error deleting record. MySQLI Error: " . mysqli_error();
}

$_SESSION['msg'] = $msg;
header("Location: index.php");
exit();

include 'bottom.inc.php'; ?>