<!-- Connects to the database.  -->
<?php
//  Set up the database connection variables
$dbHost = "localhost";
$dbUser = "root";
$dbPassword = "";
$dbName = "movies";

//  Attempt to connect to the DB server, die with error otherwise
$dbLink = mysqli_connect($dbHost, $dbUser, $dbPassword, $dbName);

if (mysqli_connect_errno())  {
    die("Could not connect to database! " . mysqli_connect_error());
}

//  Attempt to select the DB, die with error otherwise
$dbSelectOk = mysqli_select_db($dbLink, $dbName);
if (!$dbSelectOk)   {
die('Can\'t use $dbName : ' . mysqli_error());
}

$result = $dbLink->query("SELECT * FROM movies");
?>