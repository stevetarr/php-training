<!-- Add form.  -->
<?php $title = "Movie Release Information";
$page = "Add";
$header = 'Add a New Movie';
include 'top.inc.php';
$exnum = 'Add';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr';
session_start();

$movie_title = $synopsis = $release_date = $rating = "";
$titleErr = $synopErr = $dateErr = "";

if  ($_SERVER["REQUEST_METHOD"] == "POST")  {
    if(empty($_POST["movie_title"]))    {
        $titleErr = "Movie Title is required!";
    }   else {
        $movie_title = clean_input($_POST['movie_title']);
    }

    if(empty($_POST["synopsis"]))   {
        $synopErr = "Synopsis is required!";
    }   else {
        $synopsis = clean_input($_POST['synopsis']);
    }

    if(empty($_POST["$release_date"]))  {
        $dateErr = "";
    }
}   ?>


<form action="addDo.php" method="post">
    <p><span class="error">* required field</span><br/>
        <label for="movie_title">Movie Title:</label>
        <input type="text" id="movie_title" name="movie_title" value="" maxlength="80" size="40"    />
        <span class="error">*<?php echo $titleErr; ?></span>
    </p>

    <p>
        <label for="synopsis">Synopsis:</label>
    </p>
    <textarea name="synopsis" id="synopsis" rows="5" cols="40"></textarea>
    <span class="error">*<?php echo $synopErr; ?></span>
    <p>
        <label for="release_date">Release Date: </label>
        <input class="textbox-n" type="date" id="release_date" name="release_date" value=""/>
        <span class="error">*<?php echo $dateErr; ?></span>
    </p>
    <p>
        <label for="rating">Rating: </label>
        <select name="rating" id="rating">
            <option value="1">1 - Very Poor</option>
            <option value="2">2 - Poor</option>
            <option value="3">3 - OK</option>
            <option value="4">4 - Good</option>
            <option value="5">5 - Great!</option>
        </select>
    </p>
    <input type="submit" name="submit" value="Add Movie"/>
    <button type="button" onclick="window.location.href = 'index.php'">Cancel</button>
</form>

<?php include 'bottom.inc.php'; ?>
