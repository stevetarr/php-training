<?php $title = "PHP with HTML 102 - Combo Form";
$page = "formCombo";
$header = 'Form Combo';
include 'top.inc.php';
$exnum = '3 and 4';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr';

/* specific variable declaration */
$firstName = $lastName = $gender = "";
$title = $nameErr = $birthdate = $dateErr = "";

/* User entry validation and required field checking */
if  ($_SERVER["REQUEST_METHOD"] == "POST")  {
    if (empty($_POST["firstname"]))  {
        $nameErr = "First name is required";
    }   else    {
        $firstName = test_input($_POST["firstname"]);
    }

    if (empty($_POST["birthday"]))  {
        $dateErr = "Birthdate is required";
    }   else    {
        $birthdate = test_input($_POST["birthday"]);
    }

    $lastName = test_input($_POST["lastname"]);
    $title = test_input($_POST["title"]);
    $gender = test_input($_POST["gender"]);
}

/* User entry sanitization */
function test_input($data)  {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

    <!-- The input form -->
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
        <!-- The first and last name text input fields -->
        <p><span class="error">* required field</span></p>
        <label for="firstName">First Name:</label><br>
            <input type="text" name="firstname" id="firstName" value="<?php echo $firstName;?>">
        <span class="error">*<?php echo $nameErr; ?></span><br>
        <label for="lastName">Last Name:</label><br>
            <input type="text" name="lastname" id="lastName" value="<?php echo $lastName;?>"><br><br>

        <!-- The title radio buttons -->
        Title (Mr., Ms., Dr., etc.):<br>
        <label for="Mr.">Mr.</label>
            <input type="radio" name="title" <?php if (isset($title) && $title=="Mr.") echo "checked";?>
               id="Mr." value="Mr." <?php if (isset($title) && $title=="") echo "checked"; ?>><br>
        <label for="Ms.">Ms.</label>
            <input type="radio" name="title" <?php if (isset($title) && $title=="Ms.") echo "checked";?>
               id="Ms." value="Ms."><br>
        <label for="Mrs.">Mrs.</label>
            <input type="radio" name="title" <?php if (isset($title) && $title=="Mrs.") echo "checked";?>
               id="Mrs." value="Mrs."><br>
        <label for="Dr.">Dr.</label>
            <input type="radio" name="title" <?php if (isset($title) && $title=="Dr.") echo "checked";?>
               id="Dr." value="Dr."><br><br>

        <!-- The gender radio buttons -->
        Gender:<br>
        <label for="male">Male</label>
            <input type="radio" name="gender" <?php if (isset($gender) && $gender=="Male") echo "checked";?>
               id="male" value="Male" <?php if (isset($gender) && $gender=="") echo "checked"; ?>><br>
        <label for="female">Female</label>
            <input type="radio" name="gender" <?php if (isset($gender) && $gender=="Female") echo "checked";?>
               id="female" value="Female"><br>
        <label for="other">Other</label>
            <input type="radio" name="gender" <?php if (isset($gender) && $gender=="Other") echo "checked";?>
               id="other" value="Other"><br><br>

        <!-- The Birthdate month dropdown menu -->
        <label for="birthday">Birthdate: </label>
            <input class="textbox-n" type="date" id="birthday" name="birthday" value="<?php echo $birthdate;?>">
        <span class="error">*<?php echo $dateErr; ?></span><br>

        <!-- Submit button -->
        <br><input type="submit" value="Submit">
        <!-- Reset button -->
        <input type="reset">
    </form>
<!-- The form results page encapsuated in an if loop to verify that user entry has occured and the $_POST array
has been initialized. -->
<?php if (!empty($_POST) && $birthdate != "" && $firstName != "") {

    /* create a DateTime variable for the current time */
    $now = date_create();
    /* create a DateTime variable for the user entered birthday */
    $bd = $bd = date_create($birthdate);
    /* create a temp copy of the user entry for comparisons */
    $bd2 = $bd;
    /*  Print formatted name. (Title, First Name, Last Name)    */
    echo "<p></p>";
    printf(" Name: %s %s %s", $title, $firstName, $lastName);

    /*  Print formatted gender. */
    echo "<p></p>";
    printf(" Gender: %s", $gender);

    /*  Print formatted birthday.   */
    echo "<p></p>";
    printf(" Birthday %s", date_format($bd, "Y/m/d"));

    /*  Use the diff() function to compare the current date to the birthdate to get the current age */
    $diff = $now->diff($bd);
    /*  Specifically access the year variable from the diff()
    comparison to help calculate time until next birthday.    */
    $tmpAge = $diff->y;

    /*  Print the formatted age result.  */
    echo "<p></p>";
    printf(' Your age : %d years, %d months, %d days old', $tmpAge, $diff->m, $diff->d);

    /*  Use the previously defined tmpAge variable to create a DateTime object that is the user's birth month,
    birth day, and a year value that = user age + 1.    */
    $tmpAge = $tmpAge + 1;
    date_modify($bd2, "+{$tmpAge} years");

    /*  Compare the date of the user's next birthday to the current time with the diff() function.
    The result of this calculation is the time until the user's next birthday.  */
    $diff2 = $now->diff($bd2);

    /*  Print the formatted time until the next birthday result.    */
    echo "<p></p>";
    printf(' There are %d month(s) and %d day(s) left until your birthday', $diff2->m, $diff2->d);

    /* Button to clear the form completely */
    echo "<br><br><button type=\"button\" onclick=\"window.location.href = 'formCombo.php';\">Clear Results</button>";
}

include 'bottom.inc.php'; ?>