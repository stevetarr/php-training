<?php $title = "PHP with HTML 102 - Login Form";
$page = "loginForm";
$header = 'Login Form';
include 'top.inc.php';
$exnum = '5';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr';

/* specific variable declaration */
$eMail = $password = $emailErr = $passErr = "";

/* User entry validation and required field checking */
if  ($_SERVER["REQUEST_METHOD"] == "POST") {
    /*  Email entry required check   */
    if (empty($_POST["eMail"])) {
        $emailErr = "Email is required";
    }
    /*  Email entry validation    */
    else if (!filter_var($_POST["eMail"], FILTER_VALIDATE_EMAIL)) {
        $emailErr = "Email entered is not valid, please enter a valid email address.";
    }
    /*  Email entry sanitization    */
    else    {
        $eMail = test_input($_POST["eMail"]);
        $eMail = filter_var($_POST["eMail"], FILTER_SANITIZE_EMAIL);
    }

    /*  Password entry required check   */
    if (empty($_POST["password"])) {
        $passErr = "Password is required";
    }
    /*  Password entry sanitization     */
    else { //comment here 
        $password = test_input($_POST["password"]);
    }
}

/*  Function to sanitize user input     */
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

    <!--    User entry form for Email and Password  -->
    <form action="loginForm.php" method="post">
        <p><span class="error">* required field</span></p>
        <!-- Email entry    -->
        <label for="eMail">Email:</label><br>
        <input type="text" name="eMail" id="eMail" value="<?php echo $eMail;?>">
        <span class="error">*<?php echo $emailErr; ?></span><br><br>

        <!-- Password entry     -->
        <label for="password">Password:</label><br>
        <input type="password" name="password" id="password" value="<?php echo $password;?>">
        <span class="error">*<?php echo $passErr; ?></span><br><br>

        <!-- Better reset button -->
        <script>
            function customReset()  {
                document.getElementById("eMail").value = "";
                document.getElementById("password").value = "";
            }
        </script>

        <!-- Submit button -->
        <br><input type="submit" value="Submit">

        <!-- Reset button -->
        <input type="button" name="reset" value="Reset" id="resetPlus" onclick="customReset();" />
    </form>

<!-- Password Protected Content inside an if loop   -->
<?php if (!empty($_POST) && $eMail != "") {

    /* current password hashing */
    $hashedPass = password_hash("ezpass", PASSWORD_BCRYPT);

    /*  Hashed password comparisons.    */
    if (password_verify($password, $hashedPass))    {
        /* Stuff that happens if the password is correct.   */
        echo "<br><br> ACCESS GRANTED <br>";
        echo "<br> Feel free to be inspired by this nice tune. <br>";
        echo '<iframe width="1120" height="630"
            src="https://www.youtube.com/embed/wvNFiAVl-b0?&autoplay=1"
            style="border:0;" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen ></iframe>';
        echo "<br><br><button type=\"button\" onclick=\"window.location.href = 'loginForm.php';\">Clear Results</button>";
    } else  {
        /*  Stuff that happens if the password is incorrect.    */
        $passErr = "Invalid password, please try again!";
        echo '<br><span class="error">';
        echo $passErr;
        echo '</span><br>';
        echo "<br><br><button type=\"button\" onclick=\"window.location.href = 'loginForm.php';\">Clear Results</button>";
    }
} ?>

<?php include 'bottom.inc.php'; ?>