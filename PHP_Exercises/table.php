<?php $title = "PHP with HTML 101 - Exercise 3";
$page = "table";
$header = 'A Table';
include 'top.inc.php';
$exnum = '3';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr';?>

<h4>Table for the Quadratic Equation: x<sup>2</sup> - x + 3 = y</h4>

<table style="width:100%">
    <tr>
        <th scope="col">x</th>
        <th scope="col">y</th>
    </tr>
    <!-- PHP code to iterate from 1-1000, $x being the iterative variable and $y the resultant,
    this code also calculates the even or odd status using modulo 2 division. After calculation
    both the x and y values are placed in a row in their respective <td> tag.   -->
    <?php for ( $x = 1; $x <= 1000; $x++ )    {
        //If statement to determine even or odd.
        if ($x % 2 == 0)
        {
            //If the result is 0 it is even, and an even row is created.
            echo "<tr class = even>";
        } else {
            //If the result is non-zero it is odd, and an odd row is created.
            echo "<tr class = odd>";
        }
        echo "<td>".$x."</td>";
        echo "<td>".quadratic($x)."</td>";
        //The row is finished.
        echo "</tr>";
    }

    /* Easily modifiable function to calculate the result of some quadratic expression,
    here that expression is: x^2 = y    */
    function quadratic($data)
    {
        $data = ($data * $data) - $data + 3;
        return $data;
    }
    ?>
</table>
<?php include 'bottom.inc.php'; ?>
