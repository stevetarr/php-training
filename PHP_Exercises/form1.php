<?php $title = "PHP with HTML 102 - Form 1";
$page = "form1";
$header = 'Form 1';
include 'top.inc.php';
$exnum = '1 and 2';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr'; ?>

<!-- The input form -->
<form action="results1.php" method="post">
    <!-- The first and last name text input fields -->
    <label for="firstName">First Name:</label><br>
        <input type="text" name="firstname" id="firstName"><br>
    <label for="lastName">Last Name:</label><br>
        <input type="text" name="lastname" id="lastName"><br><br>

    <!-- The title radio buttons -->
    Title (Mr., Ms., Dr., etc.):<br>
    <label for="Mr.">Mr.</label>
        <input type="radio" name="title" id="Mr." value="Mr." checked><br>
    <label for="Ms.">Ms.</label>
        <input type="radio" name="title" id="Ms." value="Ms."><br>
    <label for="Mrs.">Mrs.</label>
        <input type="radio" name="title" id="Mrs." value="Mrs."><br>
    <label for="Dr.">Dr.</label>
        <input type="radio" name="title" id="Dr." value="Dr."><br><br>

    <!-- The gender radio buttons -->
    Gender:<br>
    <label for="male">Male</label>
        <input type="radio" name="gender" id="male" value="Male" checked><br>
    <label for="female">Female</label>
        <input type="radio" name="gender" id="female" value="Female"><br>
    <label for="other">Other</label>
        <input type="radio" name="gender" id="other" value="Other"><br><br>

    <!-- The Birthdate menu -->
    <label for="birthday">Birthdate: </label>
        <input class="textbox-n" type="date" id="birthday" name="birthday"><br><br>

    <!-- Submit button -->
    <input type="submit" value="Submit">
    <!-- Reset button -->
    <input type="reset">
</form>
<?php include 'bottom.inc.php'; ?>
