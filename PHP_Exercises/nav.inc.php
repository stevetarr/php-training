<?php $title = "PHP Exercise 4";
?>
<!-- <ul> element that maintains the list of links for these webpages. This list also allows for CSS modification based
on the current page and modifies the navigation bar's color to reflect that. -->
<nav>
    <ul>
        <!-- The PHP ?: operator is in use, the return value will either be active or empty based on the state of $page -->
        <li class="<?php echo ($page == "hello" ? "active" : "")?>"><a href="helloWorld.php">Hello World</a></li>
        <li class="<?php echo ($page == "date" ? "active" : "")?>"><a href="date.php">Today's Date</a></li>
        <li class="<?php echo ($page == "table" ? "active" : "")?>"><a href="table.php">Quadratic</a></li>
        <li class="<?php echo ($page == "form1" ? "active" : "")?>"><a href="form1.php">Form 1</a></li>
        <li class="<?php echo ($page == "formCombo" ? "active" : "")?>"><a href="formCombo.php">Form Combo</a></li>
        <li class="<?php echo ($page == "loginForm" ? "active" : "")?>"><a href="loginForm.php">Login Form</a></li>
    </ul>
</nav>

