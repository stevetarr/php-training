<?php $title = "PHP with HTML 102 - Results 1";
$page = "results1";
$header = 'Results 1';
include 'top.inc.php';
$exnum = '1 and 2';
$cpyrght = 'Steve Tarr, 2020';
$author = 'Steve Tarr';
$firstName = $lastName = $title = $gender = $birthday = "";

/* Assigning variables after post, then sending them to validation. */
if  ($_SERVER["REQUEST_METHOD"] == "POST")  {
    $firstName = test_input($_POST["firstname"]);
    $lastName = test_input($_POST["lastname"]);
    $title = test_input($_POST["title"]);
    $gender = test_input($_POST["gender"]);
    $birthday = test_input($_POST["birthday"]);
    }

/* Test function to remove any strange characters and prevent injection attacks */
function test_input($data)  {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

/* The form results for the name fields, in the order: Title, First Name, Last Name. */
printf(" Name: %s %s %s", $title, $firstName, $lastName);

/* The form results for the gender radio button field. */
echo "<br> Gender: " .$gender. "<br>";

/* the form results for the birthday dropdown fields. */
$bd = date_create($birthday);
/* Displaying the date results */
echo "Birthdate: " .date_format($bd, "Y/m/d")."<br><br>";

/* Utilizing the default date_create() function to compare birthdate and current time
reporting the difference between the two as the users age by access function variables directly */
$now = date_create();
$diff = $now->diff($bd);
$tmpAge = $diff->y;
printf(' Your age: %d years, %d months, %d days old.', $tmpAge, $diff->m, $diff->d); ?><br>

<!-- Incrementing the age variable used previously to count up to the next birthday. -->
<?php
$tmpAge = $tmpAge + 1;
$bd2 = $bd;
date_modify($bd2, "+{$tmpAge} years");
$diff2 = $now->diff($bd2);
printf(' There are %d month(s) and %d day(s) left until your birthday.', $diff2->m, $diff2->d);
?>
    <!-- Button to reset the form completely -->
    <br><br><button type="button" onclick="window.location.href = 'form1.php';">Back</button>
<?php include 'bottom.inc.php'; ?>