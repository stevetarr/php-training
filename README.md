###########################################################################################
#			First, select a project that you want to work with:							  #
#		 																				  #
###########################################################################################
PHP_Exercises:
	-PHP with HTML 101 - The Basics
PHP_CRUD:
	-PHP with HTML 102 - Form Exercises
	
For these two PHP with HTML projects, pull the files into whatever directory contains 
your html documents and they will work normally when their path is called. 

Example with XAMPP (C:/xampp/htdocs/php-training/(project))
###########################################################################################
training_1:
	-Laravel with Homestead.
	
For this laravel training project you must already have Virtual Box and a webhosting service
set up on the local machine (I have been using WAMP, but have tested XAMPP with success as 
well). Pull the project down into whatever project directory you like. Next, ensure that you 
have an /etc/hosts file entry for homestead.test:

Example: in (C:/Windows/System32/drivers/etc/hosts) 192.168.10.10 homestead.test

Lastly, in Git, navigate to the project directory and issue the following commands:

vendor\\bin\\homestead make

vagrant up

You should now be able to access http://homestead.test

Next, to create tables and add dummy data do the following in Git @ the repository:

php artisan migrate

php artisan tinker

factory(App\User::class, 2)->create();

factory(App\Article::class, 5)->create(['user_id' => 1]);

factory(App\Article::class, 3)->create(['user_id' => 2]);

###########################################################################################


 